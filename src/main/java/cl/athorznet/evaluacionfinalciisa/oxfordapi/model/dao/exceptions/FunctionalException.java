/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacionfinalciisa.oxfordapi.model.dao.exceptions;

/**
 *
 * @author jarqu
 */
public class FunctionalException extends Exception {

    private static final long serialVersionUID = 1L;

    private String code;

    public FunctionalException(String code, String message) {
        super(message);
        this.setCode(code);
    }

    public FunctionalException(String code, String message, Throwable cause) {
        super(message, cause);
        this.setCode(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
