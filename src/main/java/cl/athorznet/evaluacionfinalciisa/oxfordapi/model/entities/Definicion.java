/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacionfinalciisa.oxfordapi.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jarqu
 */
@Entity
@Table(name = "definiciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Definicion.findAll", query = "SELECT d FROM Definicion d"),
    @NamedQuery(name = "Definicion.findById", query = "SELECT d FROM Definicion d WHERE d.id = :id"),
    @NamedQuery(name = "Definicion.findByPalabra", query = "SELECT d FROM Definicion d WHERE d.palabra = :palabra"),
    @NamedQuery(name = "Definicion.findByDefiniciones", query = "SELECT d FROM Definicion d WHERE d.definiciones = :definiciones")})
public class Definicion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2147483647)
    @Column(name = "definiciones")
    private String definiciones;

    public Definicion() {
    }

    public Definicion(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getDefiniciones() {
        return definiciones;
    }

    public void setDefiniciones(String definiciones) {
        this.definiciones = definiciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Definicion)) {
            return false;
        }
        Definicion other = (Definicion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.athorznet.evaluacionfinalciisa.oxfordapi.model.entities.Definicion[ id=" + id + " ]\n-Palabra: "+palabra+"Significado(s): "+definiciones ;
    }
    
}
