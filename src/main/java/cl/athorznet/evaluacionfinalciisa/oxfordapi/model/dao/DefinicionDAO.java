/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacionfinalciisa.oxfordapi.model.dao;

import cl.athorznet.evaluacionfinalciisa.oxfordapi.model.dao.exceptions.NonexistentEntityException;
import cl.athorznet.evaluacionfinalciisa.oxfordapi.model.dao.exceptions.PreexistingEntityException;
import cl.athorznet.evaluacionfinalciisa.oxfordapi.model.entities.Definicion;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jarqu
 */
public class DefinicionDAO implements Serializable  {

    public DefinicionDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public DefinicionDAO() {
    }
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("EvFinal_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public Definicion create(final Definicion definicion) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(definicion);
            em.getTransaction().commit();
            } catch (final Exception ex) {
            if (findDefinicion(definicion.getId()) != null) {
                throw new PreexistingEntityException("*ERROR*: Definicion '" + definicion + "' Ya existe!.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return definicion;
    }

    public Definicion edit(Definicion definicion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            definicion = em.merge(definicion);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = definicion.getId();
                if (findDefinicion(id) == null) {
                    throw new NonexistentEntityException("*ERROR*: La definicion con id #[" + id + "] No existe!.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return definicion;
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Definicion definicion;
            try {
                definicion = em.getReference(Definicion.class, id);
                definicion.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("*ERROR*: La definicion con id #[" + id + "] No existe!.", enfe);
            }
            em.remove(definicion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Definicion> findDefinicionEntities() {
        return findDefinicionEntities(true, -1, -1);
    }

    public List<Definicion> findDefinicionEntities(int maxResults, int firstResult) {
        return findDefinicionEntities(false, maxResults, firstResult);
    }

    private List<Definicion> findDefinicionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Definicion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Definicion findDefinicion(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Definicion.class, id);
        } finally {
            em.close();
        }
    }

    public int getDefinicionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Definicion> rt = cq.from(Definicion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //Buscar palabra
    public List<Definicion> findWord(String word){
        final EntityManager em = getEntityManager();
        try {
            //ORIGINAL
            final Query q = em.createQuery("SELECT p FROM Definicion p WHERE p.palabra = '" + word + "'", Definicion.class);
            //PROBANDO...
            /*
            TypedQuery<Definicion> q = em.createNamedQuery("Definicion.findByPalabra", Definicion.class);
            q.setParameter("palabra", word);
            */
            return q.getResultList();
        } finally {
            em.close();
            System.out.println("Terminó Consulta por Palabra: '"+word+"'...");
        }     
    }    
    
}
