/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacionfinalciisa.oxfordapi.model.api;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import javax.faces.view.facelets.Metadata;
//import javax.naming.spi.DirStateFactory.Result;

/**
 *
 * @author jarqu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "metadata",
    "results",
    "word"
})
public class OxfordDef {

    @JsonProperty("id")
    public String id;
    @JsonProperty("metadata")
    public Metadata metadata;
    @JsonProperty("results")
    public List<Result> results = new ArrayList<Result>();
    @JsonProperty("word")
    public String word;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public OxfordDef withId(String id) {
        this.id = id;
        return this;
    }

    public OxfordDef withMetadata(Metadata metadata) {
        this.metadata = metadata;
        return this;
    }

    public OxfordDef withResults(List<Result> results) {
        this.results = results;
        return this;
    }

    public OxfordDef withWord(String word) {
        this.word = word;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public OxfordDef withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }
    
}
