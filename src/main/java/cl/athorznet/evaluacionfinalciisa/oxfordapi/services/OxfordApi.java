/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacionfinalciisa.oxfordapi.services;

import cl.athorznet.evaluacionfinalciisa.oxfordapi.model.api.OxfordDef;
import com.google.gson.*;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author jarqu
 */
public class OxfordApi {

    private final Logger log = Logger.getLogger(OxfordApi.class.getName());

    private final String prefixUrl = "https://od-api.oxforddictionaries.com/api/v2/entries/es/";
    private final String suffixUrl = "?fields=definitions&strictMatch=false";
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = new Gson();

    public OxfordDef getDefinitions(String word) throws IOException {

        //log.info("Environment::" + gson.toJson(System.getProperties()));
        String app_id = System.getProperty("OXFORD_APP_ID");
        String app_key = System.getProperty("OXFORD_APP_KEY");

        log.info("Request::app_id::" + app_id);
        log.info("Request::app_key::" + app_key);
        log.info("Request::Url" + prefixUrl + word + suffixUrl);
        //DATOS DE API OXFORD
        Request request = new Request.Builder()
                .url(prefixUrl + word + suffixUrl)
                .get()
                .addHeader("accept", "application/json")
                .addHeader("app_id", "001bb498")
                .addHeader("app_key", "fc735a67cdcd1b0acd4d1a4db2f7e1a1")
                //.addHeader("app_if", app_id)
                //.addHeader("app_key", app_key)
                .build();

        Response response = client.newCall(request).execute();

        OxfordDef body = gson.fromJson(response.body().string(), OxfordDef.class);

        log.info("Response::" + body.toString());

        return body;
    }

}
