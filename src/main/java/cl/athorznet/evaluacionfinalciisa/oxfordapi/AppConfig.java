/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacionfinalciisa.oxfordapi;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

/**
 *
 * @author jarqu
 */
@ApplicationPath("/api")
@OpenAPIDefinition(
        info = @Info(
                title = "Evaluación Final Apps Empresariales", 
                version = "1.0.0", 
                contact = @Contact(
                        name = "Jorge Arqueros Reyes", 
                        email = "jorge.arqueros.reyes@ciisa.cl", 
                        url = "")
                ), servers = {
                        @Server(
                                url = "/", 
                                description = "localhost")
                        }
)
public class AppConfig extends Application{
    
}
