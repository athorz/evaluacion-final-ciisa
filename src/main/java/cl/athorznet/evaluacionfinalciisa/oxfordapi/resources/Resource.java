/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.evaluacionfinalciisa.oxfordapi.resources;

import cl.athorznet.evaluacionfinalciisa.oxfordapi.model.dao.exceptions.FunctionalException;
import cl.athorznet.evaluacionfinalciisa.oxfordapi.model.dao.exceptions.PreexistingEntityException;
import cl.athorznet.evaluacionfinalciisa.oxfordapi.model.domain.ErrorResponse;
import cl.athorznet.evaluacionfinalciisa.oxfordapi.services.Service;
import java.io.IOException;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author jarqu
 */
@Path("/oxford")
public class Resource {

    Logger log = Logger.getLogger(this.getClass().getName());
    Service svc = new Service();

    @GET
    @Path("/find/{word}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findWord(@PathParam("word") final String word) {

        Object obj;
        try {
            obj = svc.findWord(word);
        } catch (IOException e) {
            ErrorResponse error = new ErrorResponse("500", e.getMessage(), e.getLocalizedMessage());
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
        } catch (FunctionalException e) {
            ErrorResponse error = new ErrorResponse("404", e.getMessage(), e.getLocalizedMessage());
            e.printStackTrace();
            return Response.status(Status.NOT_FOUND).entity(error).build();
        } catch (PreexistingEntityException e) {
            ErrorResponse error = new ErrorResponse("403", e.getMessage(), e.getLocalizedMessage());
            e.printStackTrace();
            return Response.status(Status.BAD_REQUEST).entity(error).build();
        } catch (Exception e) {
            ErrorResponse error = new ErrorResponse("500", e.getMessage(), e.getLocalizedMessage());
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
        }

        return Response.ok(obj).build();
    }

    @GET
    @Path("/history")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findHistory() {

        Object obj;
        try {
            obj = svc.findHistorial();
        } catch (Exception e) {
            ErrorResponse error = new ErrorResponse("500", e.getMessage(), e.getLocalizedMessage());
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
        }

        return Response.ok(obj).build();
    }

}
