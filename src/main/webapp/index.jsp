<%-- 
    Document   : index
    Created on : 09-05-2020, 20:07:40
    Author     : jarqu
--%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="cl.athorznet.evaluacionfinalciisa.oxfordapi.model.entities.Definicion"%>
<%@page import="cl.athorznet.evaluacionfinalciisa.oxfordapi.model.dao.DefinicionDAO"%>
<%@page import="cl.athorznet.evaluacionfinalciisa.oxfordapi.controller.DefinicionController"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="./css/estiloAthorz.css"/>
        <title>Evaluación Final JArqueros</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-info">
            <a class="navbar-brand text-white" href="index.jsp"><img class="rounded-corners img-fluid" width="100" height="100" src="https://pbs.twimg.com/profile_images/875679902216970241/NAw23Gdg_400x400.jpg"></a>

            <!--<a class="navbar-brand text-white" href="index.jsp"> Evaluación Final <br> API Oxford</a>-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!---<div class="collapse navbar-collapse" id="navbarSupportedContent">-->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"></a>
                </li>
            </ul>
            <div class="container h-100">
                <form class="form-inline my-2 my-lg-0" action="buscar" method="POST">
                    <div class="d-flex justify-content-center h-100">
                        <div class="searchbar">
                            <input class="search_input" type="text" name="txtSearch" aria-label="Search" placeholder="Ingresa la palabra a Buscar...">
                            <button class="search_icon"type="submit" name="accion" value="Buscar"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <form class="form-inline my-2 my-lg-0" action="buscar" method="POST">

                <button class="btn btn btn-warning  p-3 mb-2" type="submit" name="accion" value="Historial">Historial</button>
            </form>
            <!--</div>-->
        </nav>

        <br/>
        <c:if test="${definiciones.size() > 0}">
            <div class="mx-auto" style="width: 800px;">
                <div class="row justify-content-center">
                    <div class="col-md-12 card border-primary rounded-0">
                        <br/>
                        <h2 class="text-center">Definiciones/Significados para </h2>                        
                        <h1 class="text-center">"<b class="destaca">${palabra}</b>"</h1>
                        <br/>
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="btn-success"><h3 class="text-center">Resultados</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="def" items="${definiciones}">
                                    <tr>
                                        <th>${def}</th>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </c:if>
        <c:if test="${historial.size() > 0}">
            <div class="mx-auto" style="width: 800px;">
                <div class="row justify-content-center">
                    <div class="col-md-12 card border-primary rounded-0">
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="btn-success" colspan="3"><h3 class="text-center">Historial</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="h" items="${historial}">
                                    <tr>
                                        <th>${h.getId()}</th>
                                        <th><b class="destaca">${h.getPalabra()}</b></th>
                                        <th>${h.getDefiniciones()}</th>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </c:if>
        <footer><div class="text-dark container-fluid" style="text-align: center; margin-top: 10px;"><p><b>Programado por : Jorge Arqueros Reyes - Evaluacion FINAL CIISA - 9 de Mayo del 2020</b></p></div>
            <div class="text-center" ><img class="img-rounded img-fluid" width="200" src="https://ciisa.cl/wp-content/uploads/2017/03/LOGO_CIISA_apaisado_PNG.png"></div></footer>
    </body>
</html>
